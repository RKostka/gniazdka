package tb.sockets.server;

	import java.awt.BasicStroke;
	import java.awt.Color;
	import java.awt.Dimension;
	import java.awt.Font;
	import java.awt.Graphics;
	import java.awt.Graphics2D;
	import java.awt.RenderingHints;
	import java.awt.Toolkit;
	import java.awt.event.MouseEvent;
	import java.awt.event.MouseListener;
	import java.awt.image.BufferedImage;
	import java.io.DataInputStream;
	import java.io.DataOutputStream;
	import java.io.File;
	import java.io.IOException;
	import java.net.InetAddress;
	import java.net.ServerSocket;
	import java.net.Socket;
	import java.util.Scanner;

	import javax.imageio.ImageIO;
	import javax.swing.JFrame;
	import javax.swing.JPanel;

	public class Konsola implements Runnable {

		private JFrame okno;
		private String ip = "localhost"; 
		private int port = 6666;
		private Scanner scanner = new Scanner(System.in);
		private Thread watek;
		private final int width = 507;
		private final int height = 528;
		

		private Rysuj paint;
		private Socket sock;
		private DataOutputStream wyjscie;
		private DataInputStream wejscie;

		private ServerSocket serverSocket;

		private BufferedImage board;
		private BufferedImage krzyz;
		private BufferedImage kolko;

		private String[] pola = new String[9];

		private boolean twojRuch = false;
		private boolean circle = true;
		private boolean accepted = false;
		private boolean nieMoznaPolaczyc = false;
		private boolean wygrana = false;
		private boolean przegrana = false;
		private boolean remis = false;

		private int poleWidth = 160;
		private int errors = 0;
		private int pierwszePole = -1;
		private int drugiePole = -1;

		private Font font = new Font("Arial", Font.BOLD, 32);
		private Font malyFont = new Font("Arial", Font.BOLD, 20);
		private Font duzyFont = new Font("Arial", Font.BOLD, 50);

		private String oczekiwanieTekst = "Oczekiwanie na gracza";
		private String nieMoznaPolaczycTekst = "Nie mo�na po��czy� z graczem";
		private String wygranaTekst = "Zwyci�stwo!";
		private String przegranaTekst = "Pora�ka!";
		private String remisTekst = "Remis.";

		private int[][] wins = new int[][] { 
			{ 0, 1, 2 }, { 3, 4, 5 }, { 6, 7, 8 }, 
			{ 0, 3, 6 }, { 1, 4, 7 }, { 2, 5, 8 }, 
			{ 0, 4, 8 }, { 2, 4, 6 } };


		public Konsola() {
			System.out.println("Podaj IP: ");
			ip = scanner.nextLine();
			System.out.println("Podaj port: ");
			port = scanner.nextInt();
			while (port < 1 || port > 65535) {
				System.out.println("Podaj w�a�ciwy port: ");
				port = scanner.nextInt();
			}

			loadImages();

			paint = new Rysuj();
			paint.setPreferredSize(new Dimension(width, height));

			if (!connect()) initializeServer();

			okno = new JFrame();
			okno.setTitle("K�ko-Krzy�yk");
			okno.setContentPane(paint);
			okno.setSize(width, height);
			okno.setLocationRelativeTo(null);
			okno.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
			okno.setResizable(false);
			okno.setVisible(true);

			watek = new Thread(this, "Gra");
			watek.start();
		}

		public void run() {
			while (true) {
				tick();
				paint.repaint();

				if (!circle && !accepted) {
					listenForServerRequest();
				}

			}
		}

		private void render(Graphics g) {
			g.drawImage(board, 0, 0, null);
			if (nieMoznaPolaczyc) {
				g.setColor(Color.RED);
				g.setFont(malyFont);
				Graphics2D g2 = (Graphics2D) g;
				g2.setRenderingHint(RenderingHints.KEY_TEXT_ANTIALIASING, RenderingHints.VALUE_TEXT_ANTIALIAS_ON);
				int stringWidth = g2.getFontMetrics().stringWidth(nieMoznaPolaczycTekst);
				g.drawString(nieMoznaPolaczycTekst, width / 2 - stringWidth / 2, height / 2);
				return;
			}

			if (accepted) {
				for (int i = 0; i < pola.length; i++) {
					if (pola[i] != null) {
						if (pola[i].equals("X")) {
								g.drawImage(krzyz, (i % 3) * poleWidth + 10 * (i % 3), (int) (i / 3) * poleWidth + 10 * (int) (i / 3), null);
						} else if (pola[i].equals("O")) {
								g.drawImage(kolko, (i % 3) * poleWidth + 10 * (i % 3), (int) (i / 3) * poleWidth + 10 * (int) (i / 3), null);
						}
					}
				}
				if (wygrana || przegrana) {
					Graphics2D g2 = (Graphics2D) g;
					g2.setStroke(new BasicStroke(10));
					g.setColor(Color.BLACK);
					g.drawLine(pierwszePole % 3 * poleWidth + 10 * pierwszePole % 3 + poleWidth / 2, (int) (pierwszePole / 3) * poleWidth + 10 * (int) (pierwszePole / 3) + poleWidth / 2, drugiePole % 3 * poleWidth + 10 * drugiePole % 3 + poleWidth / 2, (int) (drugiePole / 3) * poleWidth + 10 * (int) (drugiePole / 3) + poleWidth / 2);

					g.setColor(Color.GREEN);
					g.setFont(duzyFont);
					if (wygrana) {
						int stringWidth = g2.getFontMetrics().stringWidth(wygranaTekst);
						g.drawString(wygranaTekst, width / 2 - stringWidth / 2, height / 2);
					} else if (przegrana) {
						int stringWidth = g2.getFontMetrics().stringWidth(przegranaTekst);
						g.drawString(przegranaTekst, width / 2 - stringWidth / 2, height / 2);
					}
				}
				else if (remis) {
					Graphics2D g2 = (Graphics2D) g;
					g.setColor(Color.BLACK);
					g.setFont(duzyFont);
					int stringWidth = g2.getFontMetrics().stringWidth(remisTekst);
					g.drawString(remisTekst, width / 2 - stringWidth / 2, height / 2);
				}
			} else {
				g.setColor(Color.RED);
				g.setFont(font);
				Graphics2D g2 = (Graphics2D) g;
				g2.setRenderingHint(RenderingHints.KEY_TEXT_ANTIALIASING, RenderingHints.VALUE_TEXT_ANTIALIAS_ON);
				int stringWidth = g2.getFontMetrics().stringWidth(oczekiwanieTekst);
				g.drawString(oczekiwanieTekst, width / 2 - stringWidth / 2, height / 2);
			}

		}

		private void tick() {
			if (errors >= 10) nieMoznaPolaczyc = true;

			if (!twojRuch && !nieMoznaPolaczyc) {
				try {
					int space = wejscie.readInt();
					if (circle) pola[space] = "X";
					else pola[space] = "O";
					sprawdzPrzegrana();
					sprawdzRemis();
					twojRuch = true;
				} catch (IOException e) {
					e.printStackTrace();
					errors++;
				}
			}
		}

		private void sprawdzWygrana() {
			for (int i = 0; i < wins.length; i++) {
				if (circle) {
					if (pola[wins[i][0]] == "O" && pola[wins[i][1]] == "O" && pola[wins[i][2]] == "O") {
						pierwszePole = wins[i][0];
						drugiePole = wins[i][2];
						wygrana = true;
					}
				} else {
					if (pola[wins[i][0]] == "X" && pola[wins[i][1]] == "X" && pola[wins[i][2]] == "X") {
						pierwszePole = wins[i][0];
						drugiePole = wins[i][2];
						wygrana = true;
					}
				}
			}
		}

		private void sprawdzPrzegrana() {
			for (int i = 0; i < wins.length; i++) {
				if (circle) {
					if (pola[wins[i][0]] == "X" && pola[wins[i][1]] == "X" && pola[wins[i][2]] == "X") {
						pierwszePole = wins[i][0];
						drugiePole = wins[i][2];
						przegrana = true;
					}
				} else {
					if (pola[wins[i][0]] == "O" && pola[wins[i][1]] == "O" && pola[wins[i][2]] == "O") {
						pierwszePole = wins[i][0];
						drugiePole = wins[i][2];
						przegrana = true;
					}
				}
			}
		}

		private void sprawdzRemis() {
			for (int i = 0; i < pola.length; i++) {
				if (pola[i] == null) {
					return;
				}
			}
			remis = true;
		}

		private void listenForServerRequest() {
			Socket socket = null;
			try {
				socket = serverSocket.accept();
				wyjscie = new DataOutputStream(socket.getOutputStream());
				wejscie = new DataInputStream(socket.getInputStream());
				accepted = true;
				System.out.println("Gracz do��czy�");
			} catch (IOException e) {
				e.printStackTrace();
			}
		}

		private boolean connect() {
			try {
				sock = new Socket(ip, port);
				wyjscie = new DataOutputStream(sock.getOutputStream());
				wejscie = new DataInputStream(sock.getInputStream());
				accepted = true;
			} catch (IOException e) {
				System.out.println("Uruchamianie serwera");
				return false;
			}
			System.out.println("Po��czono z serwerem");
			return true;
		}

		private void initializeServer() {
			try {
				serverSocket = new ServerSocket(port, 8, InetAddress.getByName(ip));
			} catch (Exception e) {
				e.printStackTrace();
			}
			twojRuch = true;
			circle = false;
		}

		private void loadImages() {
			try {
				board = ImageIO.read(new File("A:\\obrazy\\plansza.png")); //odpowiednie �cie�ki do obraz�w na dysku
				krzyz = ImageIO.read(new File("A:\\obrazy\\Krzyzyk.png"));
				kolko = ImageIO.read(new File("A:\\obrazy\\Kolko.png"));
			} catch (IOException e) {
				e.printStackTrace();
			}
		}

		public static void main(String[] args) {
			Konsola gra = new Konsola();
		}

		private class Rysuj extends JPanel implements MouseListener {
			private static final long serialVersionUID = 1L;

			public Rysuj() {
				setFocusable(true);
				requestFocus();
				setBackground(Color.WHITE);
				addMouseListener(this);
			}

			@Override
			public void paintComponent(Graphics g) {
				super.paintComponent(g);
				render(g);
			}

			@Override
			public void mouseClicked(MouseEvent e) {
				if (accepted) {
					if (twojRuch && !nieMoznaPolaczyc && !wygrana && !przegrana) {
						int x = e.getX() / poleWidth;
						int y = e.getY() / poleWidth;
						y *= 3;
						int position = x + y;

						if (pola[position] == null) {
							if (!circle) pola[position] = "X";
							else pola[position] = "O";
							twojRuch = false;
							repaint();
							Toolkit.getDefaultToolkit().sync();

							try {
								wyjscie.writeInt(position);
								wyjscie.flush();
							} catch (IOException e1) {
								errors++;
								e1.printStackTrace();
							}

							
							sprawdzWygrana();
							sprawdzRemis();

						}
					}
				}
			}

			@Override
			public void mousePressed(MouseEvent e) {

			}
 
			@Override
			public void mouseReleased(MouseEvent e) {

			}

			@Override
			public void mouseEntered(MouseEvent e) {

			}

			@Override
			public void mouseExited(MouseEvent e) {

			}

		}

	}



